package ab.blpbc.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

public class ClientApp {

    private static final Logger logger = LoggerFactory.getLogger(ClientApp.class);

    public static void main( String[] args ) {

        RestTemplate restTemplate = new RestTemplate();
        String url = makeRequest(42);

        logger.info("Send request at: " + LocalDateTime.now());
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        logger.info("Got response at: " + LocalDateTime.now());
        logger.info("Response: " + response);
    }

    private static String makeRequest(int n) {
        return "http://127.0.0.1:8080/fibo/" + n;
    }
}
