package ab.blpbc.async;

import ab.blpbc.async.common.AsyncCallResponse;
import org.apache.commons.lang3.builder.ToStringExclude;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.IntStream;

public class ClientAsyncCallAsyncAppTest {

    private static final Logger logger = LoggerFactory.getLogger(ClientAsyncCallAsyncAppTest.class);

    @Test
    public void test() {
        logger.info("RESULT: {}", calculate(42));
    }

    @Test
    public void testLoop() throws Exception {
        CompletableFuture.allOf(IntStream.range(0, 43)
                .mapToObj(i -> CompletableFuture.<Long>supplyAsync(() -> calculate(i)).thenAccept(l -> logger.info("fibo({})={}", i, l)))
                .toArray(CompletableFuture[]::new))
                .get();
    }

    private Long calculate(int n) {
        AsyncCallResponse result = call(requestCalculate.apply(""+n));
        String handler = result.getHandler();
        while (result.getCalculation() == null) {
            try {
                result = call(requestCheck.apply(handler));
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return result.getCalculation();
    }

    private static AsyncCallResponse call(String url) {
        RestTemplate restTemplate = new RestTemplate();
        //logger.info("request: client -> server for url: {}", url);
        AsyncCallResponse response = restTemplate.getForObject(url, AsyncCallResponse.class);
        //logger.info("response: {}", response);
        return response;
    }

    Function<String, Function<String, String>> request = method -> param -> String.format("http://127.0.0.1:8080/%s/%s", method, param);
    Function<String, String> requestCalculate = request.apply("fiboasync/calculate");
    Function<String, String> requestCheck = request.apply("fiboasync/check");
}
