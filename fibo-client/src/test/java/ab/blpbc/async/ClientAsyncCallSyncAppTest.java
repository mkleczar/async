package ab.blpbc.async;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

public class ClientAsyncCallSyncAppTest {

    private static final Logger logger = LoggerFactory.getLogger(ClientAsyncCallSyncAppTest.class);


    @Test
    public void simpleSyncCommunicationWithRestTemplate() {

        logger.info("Send request at: " + LocalDateTime.now());
        String response = call(43);
        logger.info("Got response at: " + LocalDateTime.now());
        logger.info("Response: " + response);
    }

    @Test
    public void asyncWithRestTemplate() throws Exception {
        Future<Void> f =CompletableFuture
                .supplyAsync(() -> call(42))
                .thenAccept(str -> logger.info("Response got: {}", str));
        logger.info("Request send, no blocking");

        logger.info("Start waiting");
        f.get();
        logger.info("Stop waiting");
    }

    @Test
    public void start42AsyncRequests() throws Exception {
        Executor executor = Executors.newFixedThreadPool(10);

        logger.info("Start waiting");
        CompletableFuture.allOf(IntStream.rangeClosed(1, 42)
                .peek(i -> logger.info("Compute for n={}", i))
                .mapToObj(i -> CompletableFuture.supplyAsync(() -> call(i), executor))
                .map(f -> f.thenAccept(s -> logger.info("Result: {}", s)))
                .toArray(CompletableFuture[]::new))
                .get();
        logger.info("Stop waiting");
    }


    private static String call(int n) {
        RestTemplate restTemplate = new RestTemplate();
        String url = makeRequest(n);
        logger.info("request: client -> server for n={}", n);
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        logger.info("response: client <- server for n={}", n);
        return response.getBody();
    }


    private static String makeRequest(int n) {
        return "http://127.0.0.1:8080/fibo/" + n;
    }
}
