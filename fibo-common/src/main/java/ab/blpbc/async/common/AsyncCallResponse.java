package ab.blpbc.async.common;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class AsyncCallResponse implements Serializable {
    private String result;
    private String handler;
    private Integer n;
    private Long calculation;

    public AsyncCallResponse() {
    }

    public AsyncCallResponse(String result, String handler, Integer n, Long calculation) {
        this.result = result;
        this.handler = handler;
        this.n = n;
        this.calculation = calculation;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getHandler() {
        return handler;
    }

    public void setHandler(String handler) {
        this.handler = handler;
    }

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }

    public Long getCalculation() {
        return calculation;
    }

    public void setCalculation(Long calculation) {
        this.calculation = calculation;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("result", result)
                .append("handler", handler)
                .append("n", n)
                .append("calculation", calculation)
                .toString();
    }
}
