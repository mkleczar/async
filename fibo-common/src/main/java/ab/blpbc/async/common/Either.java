package ab.blpbc.async.common;

public abstract class Either<S,T> {

    public static <S,T> Either<S,T> left(S s) {
        return new Left<>(s);
    }

    public static <S,T> Either<S,T> right(T t) {
        return new Right<>(t);
    }

    public abstract boolean isLeft();
    public boolean isRight() {return !isLeft();}
    public abstract S left();
    public abstract T right();

    private static class Left<S,T> extends Either<S,T> {
        private S s;

        private Left(S s) {
            this.s = s;
        }

        @Override
        public boolean isLeft() {
            return true;
        }

        @Override
        public S left() {
            return s;
        }

        @Override
        public T right() {
            throw new IllegalStateException("I am Left but call right()");
        }
    }

    private static class Right<S,T> extends Either<S,T> {
        private T t;

        public Right(T t) {
            this.t = t;
        }

        @Override
        public boolean isLeft() {
            return false;
        }

        @Override
        public S left() {
            throw new IllegalStateException("I am Right but call left()");
        }

        @Override
        public T right() {
            return t;
        }
    }
}
