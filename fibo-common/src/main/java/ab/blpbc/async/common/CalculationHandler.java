package ab.blpbc.async.common;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;

public class CalculationHandler<T> implements Serializable {
    private final T handler;

    protected CalculationHandler(T handler) {
        this.handler = handler;
    }

    public static <T> CalculationHandler<T> of(T t) {
        return new CalculationHandler<>(t);
    }

    public T getHandler() {
        return handler;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        CalculationHandler<?> that = (CalculationHandler<?>) o;

        return new EqualsBuilder()
                .append(handler, that.handler)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(handler)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("handler", handler)
                .toString();
    }
}
