package ab.blpbc.async.service;

import ab.blpbc.async.dao.FiboAsyncAnswer;
import ab.blpbc.async.dao.FiboAsyncHandler;

public interface FiboAsyncService {
    FiboAsyncAnswer request(int n);
    FiboAsyncAnswer result(FiboAsyncHandler handler);
}
