package ab.blpbc.async.service.async;

import ab.blpbc.async.dao.AsyncCallResult;
import ab.blpbc.async.dao.FiboAsyncHandler;
import ab.blpbc.async.dao.FiboCalculation;
import ab.blpbc.async.dao.FiboAsyncAnswer;
import ab.blpbc.async.service.FiboAsyncService;
import ab.blpbc.async.service.FiboService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class FiboAsyncRecursiveService implements FiboAsyncService {

    private static final Logger logger = LoggerFactory.getLogger(FiboAsyncRecursiveService.class);

    private static final Map<FiboAsyncHandler, FiboCalculation> resultsByHandler = new ConcurrentHashMap<>();
    private static final Map<FiboAsyncHandler, AsyncCallResult> stateByHandler = new ConcurrentHashMap<>();
    private static final Map<Integer, FiboAsyncHandler> paramToHandle = new ConcurrentHashMap<>();

    @Autowired
    private FiboService fiboService;

    @Override
    public FiboAsyncAnswer request(int n) {
        if (newParamValue(n)) {
            FiboAsyncHandler handler = getParamToHandlerMapping(n);
            storeCalculationState(handler, AsyncCallResult.IN_PROGRESS);
            CompletableFuture.supplyAsync(() -> fiboService.calculate(n))
                    .thenAccept(c -> {
                        storeCalculationResult(handler, c);
                        storeCalculationState(handler, AsyncCallResult.COMPLETED);
                    });
            return FiboAsyncAnswer.toCalculate(handler);
        }

        FiboAsyncHandler handler = getParamToHandlerMapping(n);
        AsyncCallResult state = getCalculationState(handler);

        if (state == AsyncCallResult.COMPLETED) {
            FiboCalculation calculation = getCalculationResult(handler);
            return FiboAsyncAnswer.foundInCache(calculation);
        }

        return FiboAsyncAnswer.other(handler, state);
    }

    @Override
    public FiboAsyncAnswer result(FiboAsyncHandler handler) {
        AsyncCallResult state = getCalculationState(handler);
        return state == AsyncCallResult.COMPLETED ?
                FiboAsyncAnswer.completed(getCalculationResult(handler)) :
                FiboAsyncAnswer.other(handler, state);
    }

    private boolean newParamValue(int n) {
        return !paramToHandle.containsKey(n);
    }
    private FiboAsyncHandler getParamToHandlerMapping(int n) {
        return paramToHandle.computeIfAbsent(n, i -> FiboAsyncHandler.generate());
    }
    private void storeCalculationState(FiboAsyncHandler handler, AsyncCallResult state) {
        stateByHandler.put(handler, state);
    }
    private AsyncCallResult getCalculationState(FiboAsyncHandler handler) {
        return stateByHandler.getOrDefault(handler, AsyncCallResult.UNKNOWN);
    }
    private void storeCalculationResult(FiboAsyncHandler handler, FiboCalculation calculation) {
        resultsByHandler.put(handler, calculation);
    }
    private FiboCalculation getCalculationResult(FiboAsyncHandler handler) {
        return resultsByHandler.get(handler);
    }
}
