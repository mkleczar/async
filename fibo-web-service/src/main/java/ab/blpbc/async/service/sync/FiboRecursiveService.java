package ab.blpbc.async.service.sync;

import ab.blpbc.async.dao.FiboCalculation;
import ab.blpbc.async.service.FiboService;
import org.springframework.stereotype.Service;

@Service
public class FiboRecursiveService implements FiboService {

    @Override
    public FiboCalculation calculate(int n) {
        if (n == 0) {
            return FiboCalculation.of(0, 1L);
        }
        if (n == 1) {
            return FiboCalculation.of(1, 1L);
        }
        return FiboCalculation.of(n, calculate( n-1).getNthFibo() + calculate(n-2).getNthFibo()) ;
    }
}
