package ab.blpbc.async.service;

import ab.blpbc.async.dao.FiboCalculation;

public interface FiboService {
    FiboCalculation calculate(int n);
}
