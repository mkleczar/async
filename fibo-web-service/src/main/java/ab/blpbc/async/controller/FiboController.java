package ab.blpbc.async.controller;

import ab.blpbc.async.dao.FiboCalculation;
import ab.blpbc.async.service.FiboService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/fibo")
public class FiboController {

    private static final Logger logger = LoggerFactory.getLogger(FiboController.class);

    @Autowired
    private FiboService fiboService;

    @GetMapping("/{n}")
    public FiboCalculation calculate(@PathVariable("n") int n) {
        logger.info("Serwer otrzymal zadanie {}: n={}", LocalDateTime.now(), n);
        FiboCalculation result = fiboService.calculate(n);
        logger.info("Serwer wysyla odpowiedz: {}", result);
        return result;
    }
}
