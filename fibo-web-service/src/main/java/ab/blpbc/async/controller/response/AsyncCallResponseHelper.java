package ab.blpbc.async.controller.response;

import ab.blpbc.async.common.AsyncCallResponse;
import ab.blpbc.async.dao.AsyncCallResult;
import ab.blpbc.async.dao.FiboAsyncHandler;
import ab.blpbc.async.dao.FiboCalculation;

public class AsyncCallResponseHelper {

    public static AsyncCallResponse withCalculation(FiboCalculation calculation, AsyncCallResult state) {
        return new AsyncCallResponse(state.name(), null, calculation.getN(), calculation.getNthFibo());
    }

    public static AsyncCallResponse noCalculation(FiboAsyncHandler handler, AsyncCallResult state) {
        return new AsyncCallResponse(state.name(), handler.getHandler(), null, null);
    }
}
