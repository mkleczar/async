package ab.blpbc.async.controller;

import ab.blpbc.async.common.AsyncCallResponse;
import ab.blpbc.async.controller.response.AsyncCallResponseHelper;
import ab.blpbc.async.dao.FiboAsyncHandler;
import ab.blpbc.async.dao.FiboAsyncAnswer;
import ab.blpbc.async.service.FiboAsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fiboasync")
public class FibAsyncController {

    @Autowired
    private FiboAsyncService fiboAsyncService;

    @GetMapping("/calculate/{n}")
    public AsyncCallResponse requestCalculation(@PathVariable("n") int n) {
        FiboAsyncAnswer result = fiboAsyncService.request(n);

        if (result.getAnswer().isLeft()) {
            return AsyncCallResponseHelper.withCalculation(result.getAnswer().left(), result.getResult());
        }
        return AsyncCallResponseHelper.noCalculation(result.getAnswer().right(), result.getResult());
    }

    @GetMapping("/check/{handler}")
    public AsyncCallResponse checkResult(@PathVariable("handler") String h) {
        FiboAsyncHandler handler = FiboAsyncHandler.of(h);
        FiboAsyncAnswer result = fiboAsyncService.result(handler);

        if (result.getAnswer().isLeft()) {
            return AsyncCallResponseHelper.withCalculation(result.getAnswer().left(), result.getResult());
        }
        return AsyncCallResponseHelper.noCalculation(result.getAnswer().right(), result.getResult());
    }

}
