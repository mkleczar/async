package ab.blpbc.async.dao;

import ab.blpbc.async.common.Either;

public class FiboAsyncAnswer {
    private Either<FiboCalculation, FiboAsyncHandler> answer;
    private AsyncCallResult result;

    private FiboAsyncAnswer(Either<FiboCalculation, FiboAsyncHandler> answer, AsyncCallResult result) {
        this.answer = answer;
        this.result = result;
    }

    public static FiboAsyncAnswer completed(FiboCalculation calculation) {
        return new FiboAsyncAnswer(Either.left(calculation), AsyncCallResult.COMPLETED);
    }

    public static FiboAsyncAnswer foundInCache(FiboCalculation calculation) {
        return new FiboAsyncAnswer(Either.left(calculation), AsyncCallResult.FROM_CACHE);
    }

    public static FiboAsyncAnswer toCalculate(FiboAsyncHandler handler) {
        return new FiboAsyncAnswer(Either.right(handler), AsyncCallResult.ACCEPTED);
    }

    public static FiboAsyncAnswer other(FiboAsyncHandler handler, AsyncCallResult state) {
        return new FiboAsyncAnswer(Either.right(handler), state);
    }

    public Either<FiboCalculation, FiboAsyncHandler> getAnswer() {
        return answer;
    }

    public AsyncCallResult getResult() {
        return result;
    }
}
