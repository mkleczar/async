package ab.blpbc.async.dao;

public enum AsyncCallResult {
    ACCEPTED,
    IN_PROGRESS,
    COMPLETED,
    FROM_CACHE,
    UNKNOWN,
    REJECTED
}
