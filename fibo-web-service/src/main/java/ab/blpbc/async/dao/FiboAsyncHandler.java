package ab.blpbc.async.dao;

import ab.blpbc.async.common.CalculationHandler;

import java.util.UUID;

public class FiboAsyncHandler extends CalculationHandler<String> {
    private FiboAsyncHandler(String handler) {
        super(handler);
    }

    public static FiboAsyncHandler of(String s) {
        return new FiboAsyncHandler(s);
    }

    public static FiboAsyncHandler generate() {
        return new FiboAsyncHandler(UUID.randomUUID().toString());
    }

}
