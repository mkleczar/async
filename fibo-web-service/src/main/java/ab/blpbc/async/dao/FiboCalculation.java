package ab.blpbc.async.dao;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class FiboCalculation {
    private final int n;
    private final Long nthFibo;

    private FiboCalculation(int n, Long nthFibo) {
        this.n = n;
        this.nthFibo = nthFibo;
    }

    public static FiboCalculation of(int n, Long nthFibo) {
        return new FiboCalculation(n, nthFibo);
    }

    public int getN() {
        return n;
    }

    public Long getNthFibo() {
        return nthFibo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("n", n)
                .append("nthFibo", nthFibo)
                .toString();
    }
}
